using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace PrimeNumber.ConsoleApp.Tests
{
    public class MathByStefanTests
    {
        public MathByStefanTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            _primeNumbers = new PrimeNumbers().GetNumbers();
        }

        private readonly ITestOutputHelper _testOutputHelper;
        private readonly List<int> _primeNumbers;

        [Fact]
        public void AttemptToDivideByNegativeNumberTest()
        {
            Assert.False(MathByStefan.IsPrime(-1));
        }

        [Fact]
        public void AttemptToDivideByZeroTest()
        {
            Assert.False(MathByStefan.IsPrime(0));
        }

        [Theory]
        [InlineData(4)]
        [InlineData(6)]
        [InlineData(8)]
        [InlineData(1000001)]
        public void NonePrimeNumberTest(int number)
        {
            Assert.False(MathByStefan.IsPrime(number));
        }

        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(7)]
        [InlineData(101)]
        public void PrimeNumberTest(int number)
        {
            Assert.True(MathByStefan.IsPrime(number));
        }

        [Fact]
        public void LongRunningTest()
        {
            for (var i = -2; i < 100151; i++)
            {
                var isPrime = MathByStefan.IsPrime(i);
                var isPrimeDoubleCheck = _primeNumbers.Contains(i);

                _testOutputHelper.WriteLine($"Number check {i}, isPrime {isPrime}, isPrimeDoubleCheck {isPrimeDoubleCheck}");
                Assert.Equal(isPrime, isPrimeDoubleCheck);
            }
        }

        [Fact]
        public void ShortRunningTest()
        {
            for (var i = -2; i < 101; i++)
            {
                var isPrime = MathByStefan.IsPrime(i);
                var isPrimeDoubleCheck = _primeNumbers.Contains(i);

                _testOutputHelper.WriteLine($"Number check {i}, isPrime {isPrime}, isPrimeDoubleCheck {isPrimeDoubleCheck}");
                Assert.Equal(isPrime, isPrimeDoubleCheck);
            }
        }
    }
}
