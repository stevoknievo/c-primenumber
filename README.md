# Coding-Test: Write a function to check if a number is a prime number

I always follow the "make it work, make it right, make it fast" approach to the problem... 
https://www.weeklydevtips.com/episodes/006

Here is the original request:

```
// Q1.
// Language C#
// Write a function to check if a number is prime number
class PrimeNumber {
    static bool IsPrime(int number) {
        // WRITE CODE HERE
    }
    
    static void Main() {
        int input = 1000001;
        System.Console.WriteLine(input + " is "+ (IsPrime(input) ? "Prime" : "Not Prime"));
    }
}
```

# Prime number definition
https://www.mathsisfun.com/definitions/prime-number.html

# Prime numbers source

- https://primes.utm.edu/lists/small/ (I used the small list https://primes.utm.edu/lists/small/100000.txt) -> primes-100000.txt
- http://www.mathematical.com/primes0to1000k.html -> prime-numbers-100000.csv

# Testing Duration Times (12 Tests, times after recorded at the third run)

1. 5.3357 Seconds -> if (candidate == Math.Truncate(candidate)) return false;
2. 3.1248 Seconds -> stick with integers and use modulo 
3. 2.3192 Seconds -> added potentialTestRuns
3. 1.8595 Seconds -> usd the square root from the candidate

