﻿using System;

namespace PrimeNumber.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            const int input = 1000001;
            Console.WriteLine($"{input} is {(MathByStefan.IsPrime(input) ? "Prime" : "Not Prime")}");
        }
    }
}
