using System;

namespace PrimeNumber.ConsoleApp
{
    public static class MathByStefan
    {
        /// <summary>
        ///     This method determined if the given number is a prime number.
        ///     Prime number definition:
        ///     It is a prime number when it can't be divided evenly by any number (except 1 or itself).
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Returns True or False dependent if the given number is a prime number.</returns>
        public static bool IsPrime(int number)
        {
            if (number < 2) return false;

            // http://mathandmultimedia.com/2012/06/02/determining-primes-through-square-root/
            // A square root of a number is a value that, when multiplied by itself, gives the number.
            // Example: 4 × 4 = 16, so a square root of 16 is 4.
            var testRuns = (int) Math.Sqrt(number);

            for (var i = 2; i <= testRuns; i++)
            {
                if (number % i == 0) return false; // Modulo Operator: Get Remainder From Division
            }

            return true;
        }
    }
}
